#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <utility>
#include <iostream>
#include <algorithm>
#include "omp.h"

//#define BENCH_PRINT

constexpr uint32_t NUM_THREADS = 4;
#define M_SEED 9

int32_t rows, cols;
int32_t* data;
int32_t** wall;

int32_t* result;
int32_t* result_parallel;

/**
 * Process the pathfinder problem (find shortest path in matrix) 
 * in parallel using OpenMP and test the performance gain.
*/

double parallelTime, sequentialTime;

void initializeData() {
    omp_set_num_threads(NUM_THREADS);
    data = new int32_t[rows * cols];
    wall = new int32_t*[rows];
    for (int n = 0; n < rows; n++)
    	wall[n] = data + cols * n;
    result = new int32_t[cols];
    result_parallel = new int32_t[cols];
    
    int seed = M_SEED;
    srand(seed);

    for (auto i = 0; i < rows; i++)
        for (auto j = 0; j < cols; j++)
        	wall[i][j] = rand() % 10;
	
    for (auto j = 0; j < cols; j++)
    	result[j] = result_parallel[j] = wall[0][j];
#ifdef BENCH_PRINT
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            printf("%d ",wall[i][j]);
            printf("\n");
        }
	
#endif
}

void cleanupData() {
    delete[] data;
    delete[] wall;
    //delete[] result;
    //delete[] result_parallel;
}

void processSequential() {
    int32_t *source, *destination, *tmp_destination;
    int32_t min;

    tmp_destination = destination = result;
    source = new int[cols];

    auto time = omp_get_wtime();
    for (auto t = 0; t < rows - 1; t++) {
        std::swap<int32_t*>(source, destination);
        for (auto n = 0; n < cols; n++) {
            min = source[n];
            if (n > 0)
                min = std::min(min, source[n - 1]);
            if (n < cols - 1)
                min = std::min(min, source[n + 1]);
            destination[n] = wall[t + 1][n] + min;
        }
    }
    sequentialTime = omp_get_wtime() - time;
    printf("A %dx%d matrix processed by %d threads sequentially in %.2f ms.\n", rows, cols, NUM_THREADS, sequentialTime*1000);
    if (source != tmp_destination)
        delete [] source;
}

void processParallel() {
    int32_t *source, *destination, *tmp_destination;
    int32_t min;

    tmp_destination = destination = result_parallel;
    source = new int[cols];

    auto time = omp_get_wtime();
    for (auto t = 0; t < rows - 1; t++) {
        std::swap<int32_t*>(source, destination);
        
        #pragma omp parallel for default(none)\
        schedule(dynamic, 1000) ordered\
        private(min)\
        shared(wall, source, destination, cols, t)
        for (auto n = 0; n < cols; n++) {
            min = source[n];
            if (n > 0)
                min = std::min(min, source[n - 1]);
            if (n < cols - 1)
                min = std::min(min, source[n + 1]);
            destination[n] = wall[t + 1][n] + min;
        }
    }
    parallelTime = omp_get_wtime() - time;
    printf("A %dx%d matrix processed by %d threads in parallel in %.2f ms.\n", rows, cols, NUM_THREADS, parallelTime*1000);
    if (source != tmp_destination)
    	delete [] source;
}

bool compareResults() {
	bool returnVal = true;
    for (auto i = 0; i < cols; i++) {
        if (result[i] != result_parallel[i]) {
        	 printf("%d: ", i);
        	 printf("%d %d \n", result[i], result_parallel[i]);
        	 returnVal = false;
        }
    }
    return returnVal;
}

int main(int argc, char** argv) {
    if (argc == 3) {
        cols = atoi(argv[1]);
        rows = atoi(argv[2]);
    }
    else {
    	printf("Usage: pathfinder width num_of_steps\n");
	exit(EXIT_FAILURE);
    }        
    initializeData();

    processSequential();
    processParallel();
    if (compareResults())
        std::cout << "TEST PASSED!" << std::endl;
    else
        std::cout << "TEST FAILED!" << std::endl;

    cleanupData();
#ifdef BENCH_PRINT
	for (int i = 0; i < cols; i++)
        	printf("%d ",data[i]);
	printf("\n");
    	for (int i = 0; i < cols; i++)
        	printf("%d ",dst[i]);
	printf("\n");
#endif
    return EXIT_SUCCESS;
}

