#include "omp.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <stdint.h>

#define MINREAL (-1024.0)
#define MAXREAL (1024.0)

const double ACCURACY = 0.01;

const uint32_t numberOfThreads = 8;
unsigned int threadID = ~0, startIteration = ~0, endIteration = ~0, workChunk = ~0;
bool firstDone = false, secondDone = false;

/**
 * Multiply matrices in parallel using OpenMP and test the
 * performance gain.
*/

void openMPPrep(uint32_t N) {
    if (!N || N < 0) {
        fprintf(stderr, "Error: irregular matrix size!\n");
        printf("TEST FAILED!\n");
        return;
    }
    threadID = omp_get_thread_num();
    workChunk = (N+numberOfThreads-1)/numberOfThreads;
}

void matFillSimple(long N, double *mat, double val) {
   long i, j;

   for (i = 0; i < N; i ++)
      for (j = 0; j < N; j ++)
         mat[i * N + j] = val;
}

void matFillRand(long N, double *mat) {
    long i, j;

    for (i = 0; i < N; i ++)
          for (j = 0; j < N; j ++)
             mat[i * N + j] = (rand() / (double) RAND_MAX)*(MAXREAL - MINREAL) + MINREAL;
}

void matFillTriangle(long N, double *mat) {
   long i, j;

    for (i = 0; i < N; i ++)
        for (j = 0; j < N; j ++) { 
            if (i < j) 
                mat[i * N + j] = (rand() / (double) RAND_MAX)*(MAXREAL - MINREAL) + MINREAL;
            else
                mat[i * N + j] = 0.0;
      }
}

void matMul(long N, double *a, double *b, double *c) {
   long i, j, k;
   
    if (!N || N < 0)       
        return;
   
    for (i = 0; i < N; i ++)
        for (j = 0; j < N; j ++)
            for (k = 0; k < N; k++)                
                c[i * N + j] += a[i * N + k] * b[k * N + j];  
        
    firstDone = true;
}

void matMulParallel(const long N, double *a, double *b, double *c) {
    long i, j, k;
    
    if (!N || N < 0)
        return;
      
    #pragma omp parallel default(none)\
    private(startIteration, endIteration, threadID, k)\
    shared(i, j, a, b, c, workChunk)    
    {
        threadID = omp_get_thread_num();
        startIteration = threadID * workChunk;
        endIteration = startIteration + workChunk;
        if (endIteration > N*N/numberOfThreads)
            endIteration = N*N/numberOfThreads;
        for (i = startIteration; i < endIteration; i++)
            for (j = 0; j < N; j ++)                 
                for (k = 0; k < N; k++)                         
                    c[i * N + j] += a[i * N + k] * b[k * N + j];                      
    }
    secondDone = true;
}

void runRegular(long N, double *A, double *B, double *C, double t, double* sequentialSpeed) {
    t = omp_get_wtime();
    matMul(N, A, B, C);
    *sequentialSpeed = omp_get_wtime() - t;
    printf("The %ldx%ld matrix multiplied sequentially in %f milliseconds.\n", N, N, (*sequentialSpeed)*1000);
    fflush(stdout);
}

void runParallel(const long N, double *A, double *B, double *CP, double t, double* simpleParallelSpeed) {
    t = omp_get_wtime();
    openMPPrep(N);
    matMulParallel(N, A, B, CP);
    *simpleParallelSpeed = omp_get_wtime() - t;
    printf("The %ldx%ld matrix multiplied in parallel in %f milliseconds.\n", N, N, (*simpleParallelSpeed)*1000);
    fflush(stdout);    
}

bool matricesEqual(const long N, double *A, double *B) {
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (abs(A[i*N + j] - B[i*N + j]) > ACCURACY)                
                return false;
    return true;
}

int main(int argc, char **argv) {

    long N;
    double *A, *B, *C, *CP, t, sequentialSpeed, simpleParallelSpeed, sophisticatedParallelSpeed;
    
    srand(time(NULL));

    if (argc > 1)
        N = atoi(argv[1]);
    else
        N = 256;
   
    A = (double *) malloc(N * N * sizeof(double));
    B = (double *) malloc(N * N * sizeof(double));
    C = (double *) malloc(N * N * sizeof(double));
    CP = (double *) malloc(N * N * sizeof(double));
    matFillRand(N, A);
    matFillRand(N, B);

    omp_set_num_threads(numberOfThreads);
   
    runRegular(N, A, B, C, t, &sequentialSpeed);
    runParallel(S, A, B, CP, t, &simpleParallelSpeed);       
    
    if (matricesEqual(N, C, CP)) {
       printf("TEST PASSED!\n");       
       printf("When parallel: speedup is %.2f %.\n", sequentialSpeed/simpleParallelSpeed*100);
    }
    else
       printf("TEST FAILED!\n");
    free(A);
    free(B);
    free(C);
    return EXIT_SUCCESS;
}

