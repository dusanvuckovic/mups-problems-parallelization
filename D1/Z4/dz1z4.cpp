#include <stdio.h>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <omp.h>
#include <iostream>

constexpr unsigned int numberOfThreads = 8;

/**
 * Multiply matrices in parallel using OpenMP's task option and test the
 * performance gain.
*/

//Structure holding node information
struct Node {
    int starting;
    int no_of_edges;
};

int numberOfNodes;
int edge_list_size;
FILE *fp;

//sequential data structures
Node* h_graph_nodes = nullptr;
bool *h_graph_mask = nullptr;
bool *h_updating_graph_mask = nullptr;
bool *h_graph_visited = nullptr;
int32_t *h_graph_edges = nullptr;
int32_t *h_cost = nullptr;

//parallel data structures
Node* h_graph_nodes_parallel = nullptr;
bool* h_graph_mask_parallel = nullptr;
bool* h_updating_graph_mask_parallel = nullptr;
bool* h_graph_visited_parallel = nullptr;
int32_t *h_graph_edges_parallel = nullptr;
int32_t *h_cost_parallel = nullptr;

/* Note: std::vector<bool> avoided due to the unavoidable bitwise optimization */

void readAndInitializeData(const std::string& filename) {

    //printf("Reading File\n"); //Read in graph from a file
    fp = fopen(filename.c_str(), "r");
    if (!fp) {
        printf("Error reading graph file!\n");
        printf("TEST FAILED!\n");
        exit(EXIT_FAILURE);
    }

    int source = 0, error = 1;
    error = fscanf(fp, "%d", &numberOfNodes); //read the total number of nodes
    if (!error) {
    	printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
    }

    h_graph_nodes = new Node[numberOfNodes];
    h_graph_mask = new bool[numberOfNodes];
    h_updating_graph_mask = new bool[numberOfNodes];
    h_graph_visited = new bool[numberOfNodes];
    h_cost = new int32_t[numberOfNodes];

    h_graph_nodes_parallel = new Node[numberOfNodes];
    h_graph_mask_parallel = new bool[numberOfNodes];
    h_updating_graph_mask_parallel = new bool[numberOfNodes];
    h_graph_visited_parallel = new bool[numberOfNodes];
    h_cost_parallel = new int32_t[numberOfNodes];

    unsigned int start, edgeno;
    for (auto i = 0; i < numberOfNodes; i++) {
    	error = fscanf(fp, "%d %d", &start, &edgeno);
        h_graph_nodes[i].starting = h_graph_nodes_parallel[i].starting = start;
        h_graph_nodes[i].no_of_edges = h_graph_nodes_parallel[i].no_of_edges = edgeno;
        h_graph_mask[i] = h_graph_mask_parallel[i] = false;
        h_updating_graph_mask[i] = h_updating_graph_mask_parallel[i] = false;
        h_graph_visited[i] = h_graph_visited_parallel[i] = false;
        h_cost[i] = h_cost_parallel[i] = -1;
    }
    if (!error) {
        	printf("Error in reading file!\n");
    		exit(EXIT_FAILURE);
        }

    //read the source node from the file
    error = fscanf(fp, "%d", &source);
    if (!error) {
        	printf("Error in reading file!\n");
    		exit(EXIT_FAILURE);
        }
    source = 0;

    //set the source node as true in the mask
    h_graph_mask[source] = h_graph_mask_parallel[source] = true;
    h_graph_visited[source] = h_graph_visited[source] = true;

    error = fscanf(fp, "%d", &edge_list_size);
    if (!error) {
        	printf("Error in reading file!\n");
    		exit(EXIT_FAILURE);
        }

    int32_t id, cost;
    h_graph_edges = new int32_t[edge_list_size];
    h_graph_edges_parallel = new int32_t[edge_list_size];
    for (int i = 0; i < edge_list_size; i++) {
        error = fscanf(fp, "%d", &id);
        error = fscanf(fp, "%d", &cost);
        h_graph_edges[i] = h_graph_edges_parallel[i] = id;
    }
    if (!error) {
        	printf("Error in reading file!\n");
    		exit(EXIT_FAILURE);
        }

    if (fp)
        fclose(fp);

}

void destroyData() {
    delete [] h_graph_nodes; delete [] h_graph_nodes_parallel;
    delete [] h_graph_mask; delete [] h_graph_mask_parallel;
    delete [] h_updating_graph_mask; delete [] h_updating_graph_mask_parallel;
    delete [] h_graph_visited; delete [] h_graph_visited_parallel;
    delete [] h_graph_edges; delete [] h_graph_edges_parallel;
    delete [] h_cost; delete [] h_cost_parallel;

}

void processSequential() {
    bool stop;
    do {
        stop = false; //if no thread changes this value then the loop stops

        for (auto tid = 0; tid < numberOfNodes; tid++)
            if (h_graph_mask[tid] == true) {
                h_graph_mask[tid] = false;
                for (auto i = h_graph_nodes[tid].starting; i < (h_graph_nodes[tid].no_of_edges + h_graph_nodes[tid].starting); i++) {
                    auto id = h_graph_edges[i];
                    if (!h_graph_visited[id]) {
                        h_cost[id] = h_cost[tid] + 1;
                        h_updating_graph_mask[id] = true;
                    }
                }
            }

        for (auto tid = 0; tid < numberOfNodes; tid++)
            if (h_updating_graph_mask[tid] == true) {
                h_graph_mask[tid] = true;
                h_graph_visited[tid] = true;
                stop = true;
                h_updating_graph_mask[tid] = false;
            }
    }
    while (stop);
}

void processParallel() {
    omp_set_num_threads(numberOfThreads);

    bool stop;
    #pragma omp parallel default(none)\
    shared(stop, numberOfNodes, h_graph_mask_parallel, h_graph_nodes_parallel, h_graph_edges_parallel, h_graph_visited_parallel, h_cost_parallel, h_updating_graph_mask_parallel)
    {
    #pragma omp single
    {
    do {
		#pragma omp task
        {
        stop = false; //if no thread changes this value then the loop stops
        	for (auto tid = 0; tid < numberOfNodes; tid++) {
        		if (h_graph_mask_parallel[tid] == true) {
        			h_graph_mask_parallel[tid] = false;
        			for (auto i = h_graph_nodes_parallel[tid].starting; i < (h_graph_nodes_parallel[tid].no_of_edges + h_graph_nodes_parallel[tid].starting); i++) {
        				auto id = h_graph_edges_parallel[i];
        				if (!h_graph_visited_parallel[id]) {
        					h_cost_parallel[id] = h_cost_parallel[tid] + 1;
        					h_updating_graph_mask_parallel[id] = true;
        				}
        			}
        		}
        	}
        }
		#pragma omp taskwait
		#pragma omp task
        {
           for (int tid = 0; tid < numberOfNodes; tid++) {
               if (h_updating_graph_mask_parallel[tid] == true) {
                    h_graph_mask_parallel[tid] = true;
                    h_graph_visited_parallel[tid] = true;
                    #pragma omp atomic write //alt: std::atomic<bool> stop, performanse približno iste;
                    stop = true;
                    h_updating_graph_mask_parallel[tid] = false;
               }
           }
        }
		#pragma omp taskwait
        } while(stop);
       }
    }
}

bool compareResults() {
    for (auto i = 1; i < numberOfNodes; i++)
        if (h_cost[i] != h_cost_parallel[i])
            return false;
    return true;
}

void storeToFiles() { //Store the result into files    
    FILE *fpo = fopen("resultSequential.txt", "w");
    for (auto i = 0; i < numberOfNodes; i++)
        fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
    fclose(fpo);
    printf("Result stored in resultSequential.txt\n");

    FILE *fpop = fopen("resultParallel.txt", "w");
    for (int i = 0; i < numberOfNodes; i++)
        fprintf(fpop, "%d) cost:%d\n", i, h_cost_parallel[i]);
    fclose(fpop);
    printf("Result stored in resultParallel.txt\n");
}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
    numberOfNodes = 0;
    edge_list_size = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <input_file> \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    readAndInitializeData(argv[1]);

    auto sequentialTime = omp_get_wtime();
    processSequential();
    sequentialTime = omp_get_wtime() - sequentialTime;
    std::cout << "Sequential implementation done in " << sequentialTime*1000 << " ms." << std::endl;

    auto parallelTime = omp_get_wtime();
    processParallel();
    parallelTime = omp_get_wtime() - parallelTime;
    std::cout << "Parallel implementation done in " << parallelTime*1000 << " ms." << std::endl;

    std::cout << "Speedup " << sequentialTime/parallelTime*100 << "%." << std::endl;

    if (compareResults()) {
        std::cout << "Test PASSED!" << std::endl;
    }
    else
        std::cout << "Test FAILED!" << std::endl;
    storeToFiles();
    destroyData();
}
