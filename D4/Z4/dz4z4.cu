#include <cstdint>
#include <cstdio>
#include <chrono>
#include <iostream>

/**
 * Process the pathfinder problem (find shortest path in matrix) 
 * in parallel using CUDA and test the performance gain.
*/

//host data
int32_t rows, cols;
int32_t* data;
int32_t** wall;
int32_t* result;
//host data

//device data
int32_t* data_parallel;
int32_t* result_parallel;
//device data

int32_t* result_host;

#define M_SEED 9
#define ACCURACY 10
#define MIN(a, b) ((a)<=(b) ? (a) : (b))

class Timer {

private:
	std::chrono::high_resolution_clock::time_point start, end;
	long endValue;
public:
	void startMeasurement() {
		start = std::chrono::high_resolution_clock::now();
	}

	void endMeasurement() {
		end = std::chrono::high_resolution_clock::now();
		endValue = ((std::chrono::duration_cast<std::chrono::microseconds>(
				end - start)).count());
	}

	long getMicroseconds() const {
		return endValue;
	}

};

bool compareResults() {
	bool returnVal = true;
	for (auto i = 0; i < cols; i++) {
		if (abs(result[i] - result_host[i]) > ACCURACY) {
			returnVal = false;
		}
	}
	return returnVal;
}

void initializeData() {

	//host data
	data = new int32_t[rows * cols];
	wall = new int32_t*[rows];
	for (int n = 0; n < rows; n++)
		wall[n] = data + cols * n;
	result = new int32_t[cols];
	result_host = new int32_t[cols];

	int seed = M_SEED;
	srand(seed);

	for (auto i = 0; i < rows; i++)
		for (auto j = 0; j < cols; j++)
			wall[i][j] = rand() % 10;

	for (auto j = 0; j < cols; j++)
		result[j] = wall[0][j];
	//host data

	cudaMalloc(&data_parallel, rows * cols * sizeof(int32_t));
	cudaMemcpy(data_parallel, data, rows * cols * sizeof(int32_t),
			cudaMemcpyHostToDevice);
	cudaMalloc(&result_parallel, cols * sizeof(int32_t));

	cudaMemcpy(result_parallel, data, cols * sizeof(int32_t),
			cudaMemcpyHostToDevice);
}

void destroyData() {
	delete[] data;
	delete[] result;
	delete[] result_host;

	cudaFree(data_parallel);
	cudaFree(result_parallel);
}

void processSequential() {
	int32_t *source, *destination, *tmp_destination;
	int32_t min;

	tmp_destination = destination = result;
	source = new int[cols];

	for (auto t = 0; t < rows - 1; t++) {
		std::swap<int32_t*>(source, destination);

		for (auto n = 0; n < cols; n++) {

			min = source[n];
			if (n > 0)
				min = std::min(min, source[n - 1]);
			if (n < cols - 1)
				min = std::min(min, source[n + 1]);
			destination[n] = wall[t + 1][n] + min;
		}

	}
	if (source != tmp_destination)
		delete[] source;
}

__global__ void calculateParallel(const int t, const int rows, const int cols,
		int32_t* data_parallel, int32_t* result_parallel, const int offset) {

	int threadID, n, min;
	extern __shared__ int32_t source[];
	//int32_t* destination = source + offset;

	threadID = n = blockIdx.x * blockDim.x + threadIdx.x;

	if (threadID < cols) {

		source[threadID] = result_parallel[threadID];

		min = source[threadID];
		if (n > 0)
			min = MIN(min, result_parallel[n-1]);
		if (n < cols - 1)
			min = MIN(min, result_parallel[n+1]);


		/* min = atomicMin(&source[n-1], min); //nvidia intristic function - unsupported on my GPU
		   min = atomicMin(&source[n+1], min); //
		 */
		result_parallel[n] = data_parallel[(t + 1) * cols + n] + min;

	}

}

void processParallel() {

	int threadCountPerBlock = 1024;
	int blocksNeeded = (cols + threadCountPerBlock + 1) / threadCountPerBlock;
	int myRows = rows - 1;
	if (!(rows % 2))
		myRows--;

	for (int t = 0; t < myRows; t++)
		calculateParallel<<<blocksNeeded, threadCountPerBlock, 2*cols*sizeof(int32_t)>>>
		(t, rows, cols, data_parallel, result_parallel, cols*sizeof(int32_t));
	}

int main(int argc, char** argv) {

	Timer seqTimer, parTimer;

	if (argc == 3) {
		cols = atoi(argv[1]);
		rows = atoi(argv[2]);
	} else {
		printf("Usage: pathfinder width num_of_steps\n");
		exit(EXIT_FAILURE);
	}

	initializeData();

	seqTimer.startMeasurement();
	processSequential();
	seqTimer.endMeasurement();

	parTimer.startMeasurement();
	processParallel();
	cudaDeviceSynchronize();
	parTimer.endMeasurement();

	cudaMemcpy(result_host, result_parallel, cols * sizeof(int32_t),
			cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();

	std::cout << "Processing " << rows << "x" << cols << "matrix." << std::endl;
	std::cout << "Sequential implementation: " << seqTimer.getMicroseconds()
			<< " μs." << std::endl;
	std::cout << "Parallel implementation: " << parTimer.getMicroseconds()
			<< " μs." << std::endl;
	std::cout << "Speedup: "
			<< seqTimer.getMicroseconds() / parTimer.getMicroseconds() * 100
			<< " %!" << std::endl;
	if (compareResults())
		std::cout << "Test passed!" << std::endl;
	else
		std::cout << "Test failed!" << std::endl;
	std::cout << std::endl;

}

