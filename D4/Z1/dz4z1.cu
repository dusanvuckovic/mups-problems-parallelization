#include <cstdlib>
#include <iostream>
#include <chrono>
#include <iomanip>

#define MIN -1024
#define MAX 1024

/**
* Calculate the persistence of an array of numbers usnig CUDA.
*/

float seqTime, parTime;
cudaEvent_t startC, endC;

int persistence(int x) {
	int sum, pers = 0;

	while (x >= 10) {
		sum = 0;
		while (x > 0) {
			sum += x % 10;
			x /= 10;
		}
		x = sum;
		pers++;
	}

	return pers;
}

__global__ void persistenceCUDA(int *cuda, int n) {

	int ID = blockIdx.x * blockDim.x + threadIdx.x;
	if (ID < n) {
		int x = cuda[ID];

		int sum, pers = 0;
		while (x >= 10) {
			sum = 0;
			while (x > 0) {
				sum += x % 10;
				x /= 10;
			}
			x = sum;
			pers++;
		}

		cuda[ID] = pers;
	}
}

void arrPersistance(int *in, int *out, int n) {
	int i;
	for (i = 0; i < n; i++) {
		out[i] = persistence(in[i]);
	}
}

void arrCUDAPersistance(int *cuda, int n) {

	int numberOfThreadsPerBlock = 1024;
	int numberOfBlocksNeeded = (n + numberOfThreadsPerBlock - 1)
			/ numberOfThreadsPerBlock;
	cudaEventRecord(startC, 0);
	persistenceCUDA<<<numberOfBlocksNeeded, numberOfThreadsPerBlock>>>(cuda, n);
	cudaEventRecord(endC, 0);
}

bool testArrays(const int* a, const int* b, const int n) {
	for (auto i = 0; i < n; i++)
		if (a[i] != b[i])
			return false;
	return true;
}

int main(int argc, char* argv[]) {

	int *in, *out, *result, i, n;
	int *cudaArray;

	cudaEventCreate(&startC);
	cudaEventCreate(&endC);

	srand(6);

	if (argc == 2) {
		n = atoi(argv[1]);
	} else {
		printf("N? ");
		scanf("%d", &n);
	}

	in = (int*) malloc(n * sizeof(int));
	out = (int*) malloc(n * sizeof(int));
	result = (int*) malloc(n * sizeof(int));

	for (i = 0; i < n; i++) {
		in[i] = rand() / (double) RAND_MAX * (MAX - MIN) + MIN;
	}

	cudaMalloc(&cudaArray, n * sizeof(int));
	cudaMemcpy(cudaArray, in, n * sizeof(int), cudaMemcpyHostToDevice);

	/* seq time */
	auto start = std::chrono::high_resolution_clock::now();
	arrPersistance(in, out, n);
	auto end = std::chrono::high_resolution_clock::now();
	float seqTime = ((float) (std::chrono::duration_cast
			< std::chrono::nanoseconds > (end - start)).count()) / 10e6;

	arrCUDAPersistance(cudaArray, n);

	cudaMemcpy(result, cudaArray, n * sizeof(int), cudaMemcpyDeviceToHost);

	cudaEventElapsedTime(&parTime, startC, endC);

	std::cout << "Calculating the additive persistence of " << n << " digits."
			<< std::endl;
	std::cout << "Sequential: "
			<< std::setprecision(5) << seqTime << " ms." << std::endl;
	std::cout << "Parallel: " << std::setprecision(5)
			<< parTime << " ms." << std::endl;
	std::cout << "Speedup: " << std::setprecision(5)
			<< seqTime / parTime * 100 << " %!" << std::endl;
	if (testArrays(out, result, n))
		std::cout << "Test passed!" << std::endl;
	else
		std::cout << "Test failed!" << std::endl;

	std::cout << std::endl;

	cudaEventDestroy(startC);
	cudaEventDestroy(endC);

	free(in);
	free(out);
	free(result);
	cudaFree(cudaArray);

	return 0;
}
