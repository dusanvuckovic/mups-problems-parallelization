#include <cstdio>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <omp.h>
#include <cstdint>
#include <iostream>
#include <chrono>

/**
 * Process the graph traversal problem in parallel using CUDA and test the
 * performance gain.
*/

//Structure holding node information
struct Node {
	int starting;
	int no_of_edges;
};

int numberOfNodes;
int edge_list_size;

FILE *fp;

bool *cudaStopDevice;

//sequential data structures
Node* h_graph_nodes = nullptr;
bool *h_graph_mask = nullptr;
bool *h_updating_graph_mask = nullptr;
bool *h_graph_visited = nullptr;
int32_t *h_graph_edges = nullptr;
int32_t *h_cost = nullptr;

//parallel data structures
Node* h_graph_nodes_parallel = nullptr;
bool* h_graph_mask_parallel = nullptr;
bool* h_updating_graph_mask_parallel = nullptr;
bool* h_graph_visited_parallel = nullptr;
int32_t* h_graph_edges_parallel = nullptr;
int32_t* h_cost_parallel = nullptr;

int32_t* h_cost_result = nullptr;

/* Note: std::vector<bool> avoided due to the bitwise optimization */

void readAndInitializeData(const std::string& filename) {

	//printf("Reading File\n"); //Read in graph from a file
	fp = fopen(filename.c_str(), "r");
	if (!fp) {
		printf("Error reading graph file!\n");
		printf("TEST FAILED!\n");
		exit(EXIT_FAILURE);
	}

	int source = 0, error = 1;
	error = fscanf(fp, "%d", &numberOfNodes); //read the total number of nodes
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	h_graph_nodes = (Node*) malloc(numberOfNodes * sizeof(Node));
	h_graph_mask = new bool[numberOfNodes];
	h_updating_graph_mask = new bool[numberOfNodes];
	h_graph_visited = new bool[numberOfNodes];
	h_cost = new int32_t[numberOfNodes];

	cudaMalloc(&h_graph_nodes_parallel, numberOfNodes * sizeof(Node));
	cudaMalloc(&h_graph_mask_parallel, numberOfNodes * sizeof(bool));
	cudaMalloc(&h_updating_graph_mask_parallel, numberOfNodes * sizeof(bool));
	cudaMalloc(&h_graph_visited_parallel, numberOfNodes * sizeof(bool));
	cudaMalloc(&h_cost_parallel, numberOfNodes * sizeof(int32_t));

	unsigned int start, edgeno;
	for (auto i = 0; i < numberOfNodes; i++) {
		error = fscanf(fp, "%d %d", &start, &edgeno);
		h_graph_nodes[i].starting = start;
		h_graph_nodes[i].no_of_edges = edgeno;
		h_graph_mask[i] = false;
		h_updating_graph_mask[i] = false;
		h_graph_visited[i] = false;
		h_cost[i] = -1;
	}
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	//read the source node from the file
	error = fscanf(fp, "%d", &source);
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}
	source = 0;

	//set the source node as true in the mask
	h_graph_mask[source] = true;
	h_graph_visited[source] = true;

	error = fscanf(fp, "%d", &edge_list_size);
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	int32_t id, cost;
	h_graph_edges = new int32_t[edge_list_size];
	cudaMalloc(&h_graph_edges_parallel, edge_list_size * sizeof(int32_t));
	for (int i = 0; i < edge_list_size; i++) {
		error = fscanf(fp, "%d", &id);
		error = fscanf(fp, "%d", &cost);
		h_graph_edges[i] = id;
	}
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	if (fp)
		fclose(fp);


	h_cost_result = new int32_t[numberOfNodes];

	cudaMemcpy(h_graph_nodes_parallel, h_graph_nodes,
			numberOfNodes * sizeof(Node), cudaMemcpyHostToDevice);
	cudaMemcpy(h_graph_mask_parallel, h_graph_mask,
			numberOfNodes * sizeof(bool), cudaMemcpyHostToDevice);
	cudaMemcpy(h_updating_graph_mask_parallel, h_updating_graph_mask,
			numberOfNodes * sizeof(bool), cudaMemcpyHostToDevice);
	cudaMemcpy(h_graph_visited_parallel, h_graph_visited,
			numberOfNodes * sizeof(bool), cudaMemcpyHostToDevice);
	cudaMemcpy(h_cost_parallel, h_cost, numberOfNodes * sizeof(int32_t),
			cudaMemcpyHostToDevice);
	cudaMemcpy(h_graph_edges_parallel, h_graph_edges,
			edge_list_size * sizeof(int32_t), cudaMemcpyHostToDevice);

}

void destroyData() {
	delete[] h_graph_nodes;
	delete[] h_graph_mask;
	delete[] h_updating_graph_mask;
	delete[] h_graph_visited;
	delete[] h_graph_edges;
	delete[] h_cost;

	cudaFree(h_graph_nodes_parallel);
	cudaFree(h_graph_mask_parallel);
	cudaFree(h_updating_graph_mask_parallel);
	cudaFree(h_graph_visited_parallel);
	cudaFree(h_graph_edges_parallel);
	cudaFree(h_cost_parallel);

	delete[] h_cost_result;
	cudaFree(cudaStopDevice);
}

void processSequential() {
	bool stop;
	do {
		stop = false; //if no thread changes this value then the loop stops

		for (auto tid = 0; tid < numberOfNodes; tid++)
			if (h_graph_mask[tid] == true) {
				h_graph_mask[tid] = false;
				for (auto i = h_graph_nodes[tid].starting;
						i
								< (h_graph_nodes[tid].no_of_edges
										+ h_graph_nodes[tid].starting); i++) {
					auto id = h_graph_edges[i];
					if (!h_graph_visited[id]) {
						h_cost[id] = h_cost[tid] + 1;
						h_updating_graph_mask[id] = true;
					}
				}
			}

		for (auto tid = 0; tid < numberOfNodes; tid++)
			if (h_updating_graph_mask[tid] == true) {
				h_graph_mask[tid] = true;
				h_graph_visited[tid] = true;
				stop = true;
				h_updating_graph_mask[tid] = false;
			}
	} while (stop);
}

__global__ void cudaFirst(const int numberOfNodes, const int edge_list_size,
		Node* h_graph_nodes_parallel, bool* h_graph_mask_parallel,
		bool* h_updating_graph_mask_parallel, bool* h_graph_visited_parallel,
		int32_t* h_graph_edges_parallel, int32_t* h_cost_parallel) {

	unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid < numberOfNodes) {
		if (h_graph_mask_parallel[tid] == true) {
			h_graph_mask_parallel[tid] = false;
			for (auto i = h_graph_nodes_parallel[tid].starting;
					i < (h_graph_nodes_parallel[tid].no_of_edges + h_graph_nodes_parallel[tid].starting); i++) {
				auto id = h_graph_edges_parallel[i];
				if (!h_graph_visited_parallel[id]) {
					h_cost_parallel[id] = h_cost_parallel[tid] + 1;
					h_updating_graph_mask_parallel[id] = true;
				}
			}
		}
	}
}

__global__ void cudaSecond(const int numberOfNodes, bool *h_graph_mask_parallel,
		bool* h_updating_graph_mask_parallel, bool *h_graph_visited_parallel, bool* cudaStopDevice) {

	unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid < numberOfNodes) {
		if (h_updating_graph_mask_parallel[tid] == true) {
			h_graph_mask_parallel[tid] = true;
			h_graph_visited_parallel[tid] = true;
			*cudaStopDevice = true;
			h_updating_graph_mask_parallel[tid] = false;
		}
	}
}

void processParallel() {

	int numberOfThreadsPerBlock = 1024;
	int numberOfBlocksNeeded = (numberOfNodes + numberOfThreadsPerBlock - 1)
			/ numberOfThreadsPerBlock;

	bool cudaStopHost;
	cudaMalloc(&cudaStopDevice, sizeof(bool));
	cudaMemset(cudaStopDevice, 0, sizeof(bool));

	do {
		cudaStopHost = false; //if no thread changes this value then the loop stops
		cudaMemset(cudaStopDevice, 0, sizeof(bool));

		cudaFirst<<<numberOfBlocksNeeded, numberOfThreadsPerBlock>>>(numberOfNodes, edge_list_size,
				h_graph_nodes_parallel, h_graph_mask_parallel, h_updating_graph_mask_parallel,
				h_graph_visited_parallel, h_graph_edges_parallel, h_cost_parallel);

		cudaSecond<<<numberOfBlocksNeeded, numberOfThreadsPerBlock>>>(numberOfNodes,
				h_graph_mask_parallel, h_updating_graph_mask_parallel, h_graph_visited_parallel,
						cudaStopDevice);

		cudaMemcpy(&cudaStopHost, cudaStopDevice, sizeof(bool), cudaMemcpyDeviceToHost);

	} while (cudaStopHost);

	cudaMemcpy(h_cost_result, h_cost_parallel, sizeof(int32_t) * numberOfNodes, cudaMemcpyDeviceToHost);
}

bool compareResults() {
	for (auto i = 1; i < numberOfNodes; i++)
		if (h_cost[i] != h_cost_result[i])
			return false;
	return true;
}

void storeToFiles() { //Store the result into files
	FILE *fpo = fopen("resultSequential.txt", "w");
	for (auto i = 0; i < numberOfNodes; i++)
		fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
	fclose(fpo);

	FILE *fpop = fopen("resultParallel.txt", "w");
	for (int i = 0; i < numberOfNodes; i++)
		fprintf(fpop, "%d) cost:%d\n", i, h_cost_result[i]);
	fclose(fpop);
}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
	numberOfNodes = 0;
	edge_list_size = 0;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_file> \n", argv[0]);
		exit(EXIT_FAILURE);
	}

	readAndInitializeData(argv[1]);

	auto start = std::chrono::high_resolution_clock::now();
	processSequential();
	auto end = std::chrono::high_resolution_clock::now();
	auto seqTime = (std::chrono::duration_cast<std::chrono::microseconds>(
			end - start)).count();

	auto startP = std::chrono::high_resolution_clock::now();
	processParallel();
	cudaDeviceSynchronize();
	auto endP = std::chrono::high_resolution_clock::now();
	auto parTime = ((std::chrono::duration_cast<std::chrono::microseconds>(
			endP - startP)).count());

	std::cout << "For the input file "<< argv[1] << ":" << std::endl;

	std::cout << "Sequential implementation: " << seqTime
			<< " μs." << std::endl;

	std::cout << "Parallel implementation: " << std::fixed
			<< parTime << " μs." << std::endl;

	std::cout << "Speedup: " << seqTime / parTime * 100 << "%."
			<< std::endl;

	if (compareResults()) {
		std::cout << "Test PASSED!" << std::endl;
	} else
		std::cout << "Test FAILED!" << std::endl;
	std::cout << std::endl;

	storeToFiles();
	destroyData();
}
