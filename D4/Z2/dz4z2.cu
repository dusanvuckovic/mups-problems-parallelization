#include <cstdlib>
#include <iostream>
#include <chrono>
#include <iomanip>

#define MIN -1024
#define MAX 1024

/**
* Calculate the persistence of a matrix of numbers usnig CUDA.
*/


int persistence(int x) {
	int sum, pers = 0;

	while (x >= 10) {
		sum = 0;
		while (x > 0) {
			sum += x % 10;
			x /= 10;
		}
		x = sum;
		pers++;
	}

	return pers;
}

__global__ void persistenceCUDA(int *cuda, int limit) {

	int ID = blockIdx.y * blockDim.y * blockDim.x + blockIdx.x * blockDim.x
			+ threadIdx.x;
	if (ID < limit) {
		int x = cuda[ID];

		int sum, pers = 0;
		while (x >= 10) {
			sum = 0;
			while (x > 0) {
				sum += x % 10;
				x /= 10;
			}
			x = sum;
			pers++;
		}
		cuda[ID] = pers;
	}
}

void arrPersistance(int *in, int *out, int m, int n) {
	int i;
	for (i = 0; i < n * m; i++) {
		out[i] = persistence(in[i]);
	}
}

void arrCUDAPersistance(int *cuda, int m, int n) {
	int limit = m * n;

	int numberOfThreadsPerBlock = 1024; //const, blok je 1d

	int numberOfBlocksHorizontal = (m + numberOfThreadsPerBlock + 1)
			/ numberOfThreadsPerBlock;
	int numberOfBlocksVertical = n;

	dim3 myGrid(numberOfBlocksHorizontal, numberOfBlocksVertical);

	persistenceCUDA<<<myGrid, numberOfThreadsPerBlock>>>(cuda, limit);

}

bool testArrays(const int* a, const int* b, const int n) {
	for (auto i = 0; i < n; i++)
		if (a[i] != b[i])
			return false;
	return true;
}

int main(int argc, char* argv[]) {

	int *in, *out, *result, i, m, n;
	int *cudaArray;

	srand(time(NULL));

	if (argc == 3) {
		m = atoi(argv[1]);
		n = atoi(argv[2]);
	} else {
		printf("M?");
		scanf("%d", &m);
		printf("N?");
		scanf("%d", &n);
	}

	in = (int*) malloc(n * m * sizeof(int));
	out = (int*) malloc(n * m * sizeof(int));
	result = (int*) malloc(n * m * sizeof(int));

	for (i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			in[i * m + n] = rand() / (double) RAND_MAX * (MAX - MIN) + MIN;
	}

	cudaMalloc(&cudaArray, n * sizeof(int));
	cudaMemcpy(cudaArray, in, n * sizeof(int), cudaMemcpyHostToDevice);

	/* seq time */
	auto start = std::chrono::high_resolution_clock::now();
	arrPersistance(in, out, n, m);
	auto end = std::chrono::high_resolution_clock::now();
	auto seqTime = (std::chrono::duration_cast<std::chrono::microseconds>(end - start)).count();

	auto startP = std::chrono::high_resolution_clock::now();
	arrCUDAPersistance(cudaArray, n, m);
	cudaDeviceSynchronize();
	auto endP = std::chrono::high_resolution_clock::now();
	auto parTime = ((std::chrono::duration_cast<
			std::chrono::microseconds>(endP - startP)).count());

	cudaMemcpy(result, cudaArray, n * sizeof(int), cudaMemcpyDeviceToHost);

	std::cout << "Calculating the additive persistence of a " << m << "x" << n
			<< "matrix." << std::endl;
	std::cout << "Sequential: " << std::fixed << seqTime << " μs."
			<< std::endl;
	std::cout << "Parallel: " << std::fixed << parTime << " μs."
			<< std::endl;
	std::cout << "Speedup: " << std::fixed << seqTime / parTime * 100 << " %!"
			<< std::endl;
	if (testArrays(out, result, n))
		std::cout << "Test passed!" << std::endl;
	else
		std::cout << "Test failed!" << std::endl;

	std::cout << std::endl;

	free(in);
	free(out);
	free(result);
	cudaFree(cudaArray);

	return 0;
}
