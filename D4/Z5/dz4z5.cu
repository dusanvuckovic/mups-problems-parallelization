#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <cmath>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sys/types.h>
#include <fcntl.h>
#include <omp.h>
#include <unistd.h>
#include <vector>
#include <chrono>
#include <iostream>

/**
 * K-means cluster in parallel using CUDA and test the
 * performance gain.
*/

#define RANDOM_MAX 2147483647

#ifndef FLT_MAX
#define FLT_MAX 3.40282347e+38
#endif

#define ACCURACY 2

class Timer {

private:
	std::chrono::high_resolution_clock::time_point start, end;
	long endValue;
public:
	void startMeasurement() {
		start = std::chrono::high_resolution_clock::now();
	}

	void endMeasurement() {
		end = std::chrono::high_resolution_clock::now();
		endValue = ((std::chrono::duration_cast<std::chrono::milliseconds>(
				end - start)).count());
	}

	long getMilliseconds() const {
		return endValue;
	}

};

std::vector<std::vector<float>> cluster(int numObjects, int numAttributes,
		std::vector<std::vector<float>>& attributes, int numClusters,
		float threshold);

std::vector<std::vector<float>> cluster_parallel(int numObjects,
		int numAttributes, std::vector<std::vector<float>>& attributes,
		int numClusters, float threshold);

/*----< euclid_dist_2() >----------------------------------------------------*/
/* multi-dimensional spatial Euclid distance square */
float euclid_dist_2(std::vector<float>& object1, std::vector<float>& object2,
		int numAttributes) {
	int i;
	float ans = 0.0;

	for (i = 0; i < numAttributes; i++)
		ans += (object1[i] - object2[i]) * (object1[i] - object2[i]);

	return (ans);
}

int find_nearest_point(std::vector<std::vector<float>>& attributes,
		int numAttributes, std::vector<std::vector<float>>& clusters,
		int numClusters, int iter) {
	int index, i;
	float min_dist = FLT_MAX;
	std::vector<float>& attribute = attributes[iter];

	for (i = 0; i < numClusters; i++) {

		float dist;
		std::vector<float>& cluster = clusters[i];
		dist = euclid_dist_2(attribute, cluster, numAttributes);
		if (dist < min_dist) {
			min_dist = dist;
			index = i;
		}
	}
	return (index);

}

/*----< kmeans_clustering() >---------------------------------------------*/
std::vector<std::vector<float>> kmeans_clustering(
		std::vector<std::vector<float>>& attributes, /* in: [npoints][nfeatures] */
		int numAttributes, int numObjects, int numClusters, float threshold,
		std::vector<int>& membership) /* out: [npoints] */
		{

	int i, j, n = 0, index;
	float delta;

	std::vector<int> new_centers_len; /* [nclusters]: no. of points in each cluster */
	std::vector<std::vector<float>> clusters; /* out: [nclusters][nfeatures] */
	std::vector<std::vector<float>> new_centers; /* [nclusters][nfeatures] */

	clusters.resize(numClusters);
	for (auto& v : clusters)
		v.resize(numAttributes);

	/* randomly pick cluster centers */
	for (i = 0; i < numClusters; i++) {
		//n = (int)rand() % npoints;
		for (j = 0; j < numAttributes; j++)
			clusters[i][j] = attributes[n][j];
		n++;
	}

	membership.resize(numObjects, -1);

	/* need to initialize new_centers_len and new_centers[0] to all 0 */
	new_centers_len.resize(numClusters, 0);
	new_centers.resize(numClusters);
	for (auto& center : new_centers)
		center.resize(numAttributes, 0);

	do {

		delta = 0.0;
		for (i = 0; i < numObjects; i++) {
			/* find the index of nestest cluster centers */
			index = find_nearest_point(attributes, numAttributes, clusters,
					numClusters, i);

			/* if membership changes, increase delta by 1 */
			if (membership[i] != index)
				delta += 1.0;

			/* assign the membership to object i */
			membership[i] = index;

			/* update new cluster centers : sum of objects located within */
			new_centers_len[index]++;
			for (j = 0; j < numAttributes; j++)
				new_centers[index][j] += attributes[i][j];
		}

		/* replace old cluster centers with new_centers */
		for (i = 0; i < numClusters; i++) {
			for (j = 0; j < numAttributes; j++) {
				if (new_centers_len[i] > 0)
					clusters[i][j] = new_centers[i][j] / new_centers_len[i];
				new_centers[i][j] = 0.0; /* set back to 0 */
			}
			new_centers_len[i] = 0; /* set back to 0 */
		}

		//delta /= npoints;

	} while (delta > threshold);

	return clusters;
}


__global__ void find_nearest_point_2d(int numObjects, float *attributes,
		int numAttributes, float *clusters, int numClusters, int* indices) {

	//x: numObjects; y: numClusters;

	int index = 0;
	float min_dist = FLT_MAX;

	int blockID = blockIdx.x;
	int threadID = blockID * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;

	if (threadID < numObjects * numClusters) {
		float *pt1 = attributes + numAttributes * threadIdx.x;
		float *pt2 = clusters + numAttributes * threadIdx.y;
		float dist = 0.0;
		for (int j = 0; j < numAttributes; j++)
			dist += (pt1[j] - pt2[j]) * (pt1[j] - pt2[j]);

		if (dist < min_dist) {
			min_dist = dist;
			index = threadIdx.y;
		}
		indices[threadIdx.x] = index; // indices.size() == numObjects here

	}

}

__global__ void find_nearest_point(int numObjects, float *attributes,
		int numAttributes, float *clusters, int numClusters, int* indices) {

	int index, threadID;
	float min_dist = FLT_MAX;

	threadID = blockIdx.x * blockDim.x + threadIdx.x;
	float *pt1 = attributes + numAttributes * threadID;

	if (threadID < numObjects) {

		for (int i = 0; i < numClusters; i++) {

			float *pt2 = clusters + numAttributes * i;
			float dist = 0.0;

			for (int j = 0; j < numAttributes; j++)
				dist += (pt1[j] - pt2[j]) * (pt1[j] - pt2[j]);

			if (dist < min_dist) {
				min_dist = dist;
				index = i;
			}
			indices[threadID] = index; // indices.size() == numObjects here
		}
	}
}

__global__ void assignMembership(const int numObjects,
		const int* __restrict__ indices, int* __restrict__ membership,
		float *cudaDelta, int* __restrict__ new_centers_len, int numAttributes,
		float* __restrict__ new_centers, const float *__restrict__ attributes) {

	int threadID, i;

	threadID = i = blockIdx.x * blockDim.x + threadIdx.x;

	if (threadID < numObjects) {
		int index = indices[i];
		if (membership[i] != index)
			*cudaDelta += 1.0;
		membership[i] = index; /* assign the membership to object i */

		/* update new cluster centers : sum of objects located within */
		atomicAdd(&new_centers_len[index], 1);
		for (int j = 0; j < numAttributes; j++)
			atomicAdd(&new_centers[index * numAttributes + j],
					attributes[i * numAttributes + j]);

	}

}

__global__ void replaceCenters(int numClusters, int numAttributes,
		int * __restrict__ new_centers_len, float * __restrict__ clusters,
		float * __restrict__ new_centers) {

	int threadID, i;

	threadID = i = blockIdx.x * blockDim.x + threadIdx.x;

	if (threadID < numClusters) {
		for (int j = 0; j < numAttributes; j++) {
			if (new_centers_len[i] > 0)
				clusters[i * numAttributes + j] = new_centers[i * numAttributes
						+ j] / new_centers_len[i];
			new_centers[i * numAttributes + j] = 0.0; /* set back to 0 */
		}
		new_centers_len[i] = 0; /* set back to 0 */
	}

}

__global__ void printout(int numObjects, int numClusters, int numAttributes,
		float * attributes, float *clusters, float *new_centers, int *indices,
		int *membership, int *new_centers_len) {
	printf("\nCUDA ATTRIBUTES!\n");
	for (int i = 0; i < numClusters; i++) {
		for (int j = 0; j < numAttributes; j++)
			printf("%.2f ", attributes[i * numAttributes + j]);
		printf("\n");
	}
	printf("\nCUDA CLUSTERS!\n");
	for (int i = 0; i < numClusters; i++) {
		for (int j = 0; j < numAttributes; j++)
			printf("%.2f ", clusters[i * numAttributes + j]);
		printf("\n");
	}
	printf("\nCUDA NEW_CENTERS!\n");
	for (int i = 0; i < numClusters; i++) {
		for (int j = 0; j < numAttributes; j++)
			printf("%.2f ", new_centers[i * numAttributes + j]);
		printf("\n");
	}
	printf("\nCUDA INDICES!\n");
	for (int i = 0; i < numObjects; i++) {
		printf("%d ", indices[i]);
	}
	printf("\nCUDA MEMBERSHIP!\n");
	for (int i = 0; i < numObjects; i++) {
		printf("%d ", membership[i]);
	}
	printf("\nCUDA NEW_CENTERS_LEN!\n");
	for (int i = 0; i < numClusters; i++) {
		printf("%d ", new_centers_len[i]);
	}

}

/*----< kmeans_clustering() >---------------------------------------------*/
std::vector<std::vector<float>> kmeans_clustering_parallel(
		std::vector<std::vector<float>>& attributes, int numAttributes,
		int numObjects, int numClusters, float threshold,
		std::vector<int>& membership) {

	int i, j, n = 0;
	float delta;

	std::vector<int> new_centers_len; /* [nclusters]: no. of points in each cluster */
	std::vector<std::vector<float>> clusters; /* out: [nclusters][nfeatures] */
	std::vector<std::vector<float>> new_centers; /* [nclusters][nfeatures] */
	std::vector<int> indices;

	indices.resize(numObjects);

	clusters.resize(numClusters);
	for (auto& v : clusters)
		v.resize(numAttributes);

	/* randomly pick cluster centers */
	for (i = 0; i < numClusters; i++) {
		for (j = 0; j < numAttributes; j++)
			clusters[i][j] = attributes[n][j];
		n++;
	}

	membership.resize(numObjects, -1);

	new_centers_len.resize(numClusters, 0);
	new_centers.resize(numClusters);
	for (auto& center : new_centers)
		center.resize(numAttributes, 0);

	int *indicesC;
	float *attributesC;
	float *clustersC;
	int *membershipC;
	int *new_centers_lenC;
	float *new_centersC;

	float *cudaDeltaDevice;
	cudaMalloc(&cudaDeltaDevice, sizeof(float));
	cudaMemset(cudaDeltaDevice, 0, sizeof(float));

	cudaMalloc(&indicesC, indices.size() * sizeof(int));
	cudaMemcpy(indicesC, indices.data(), indices.size() * sizeof(int),
			cudaMemcpyHostToDevice);

	int attributesSize = 0;
	for (auto& w : attributes)
		attributesSize += w.size();
	cudaMalloc(&attributesC, attributesSize * sizeof(float));
	int attributesOffset = 0;
	for (auto& w : attributes) {
		cudaMemcpy(attributesC + attributesOffset, w.data(),
				w.size() * sizeof(float), cudaMemcpyHostToDevice);
		attributesOffset += w.size();
	}

	int clustersSize = 0;
	for (auto& w : clusters)
		clustersSize += w.size();
	cudaMalloc(&clustersC, clustersSize * sizeof(float));
	int clustersOffset = 0;
	for (auto &w : clusters) {
		cudaMemcpy(clustersC + clustersOffset, w.data(),
				w.size() * sizeof(float), cudaMemcpyHostToDevice);
		clustersOffset += w.size();
	}

	cudaMalloc(&membershipC, membership.size() * sizeof(int));
	cudaMemcpy(membershipC, membership.data(), membership.size() * sizeof(int),
			cudaMemcpyHostToDevice);

	cudaMalloc(&new_centers_lenC, new_centers_len.size() * sizeof(int));
	cudaMemcpy(new_centers_lenC, new_centers_len.data(),
			new_centers_len.size() * sizeof(int), cudaMemcpyHostToDevice);

	int newCentersSize = 0;
	for (auto& w : new_centers)
		newCentersSize += w.size();
	cudaMalloc(&new_centersC, attributesSize * sizeof(float));
	int newCentersOffset = 0;
	for (auto& w : new_centers) {
		cudaMemcpy(new_centersC + newCentersOffset, w.data(),
				w.size() * sizeof(float), cudaMemcpyHostToDevice);
		newCentersOffset += w.size();
	}


	int threadCountPerBlock = 1024;
	int objectThreads = (numObjects + threadCountPerBlock - 1)
			/ threadCountPerBlock;
	int clusterThreads = (numClusters + threadCountPerBlock - 1)
			/ threadCountPerBlock;


	int objectThreadsPerBlock = 32;
	int clusterThreadsPerBlock = 32;
	dim3 v(objectThreadsPerBlock, clusterThreadsPerBlock);

	int howMany2DBlocks = (numObjects * numClusters + threadCountPerBlock -1) / threadCountPerBlock;


	do {
		cudaMemset(cudaDeltaDevice, 0.0, sizeof(float));

		find_nearest_point<<<objectThreads, threadCountPerBlock>>>(numObjects,
				attributesC, numAttributes,
				clustersC, numClusters,
				indicesC);

		/*find_nearest_point_2d<<<howMany2DBlocks, threadCountPerBlock>>>(numObjects,
				attributesC, numAttributes,
				clustersC, numClusters,
				indicesC);*/

		assignMembership<<<objectThreads, threadCountPerBlock>>>(numObjects, indicesC,
				membershipC, cudaDeltaDevice, new_centers_lenC, numAttributes,
				new_centersC, attributesC);

		replaceCenters<<<clusterThreads, threadCountPerBlock>>>
		(numClusters, numAttributes, new_centers_lenC, clustersC, new_centersC);

		cudaMemcpy(&delta, cudaDeltaDevice, sizeof(float),
				cudaMemcpyDeviceToHost);

	} while (delta > threshold);

	int currentOffset = 0;
	for (auto& w : clusters) {
		cudaMemcpy(w.data(), clustersC + currentOffset,
				w.size() * sizeof(float), cudaMemcpyDeviceToHost);
		currentOffset += w.size();
	}

	return clusters;
}

/*---< usage() >------------------------------------------------------------*/
void usage(char *argv0) {
	std::string help = "Usage: %s [switches] -i filename\n"
			"       -i filename     :  file containing data to be clustered\n"
			"       -b                 :input file is in binary format\n"
			"       -k                 : number of clusters (default is 8) \n"
			"       -t threshold    : threshold value\n";
	fprintf(stderr, help.c_str(), argv0);
	exit(-1);
}

/*---< isTestPassed() >-----------------------------------------------------*/
bool isTestPassed(int numClusters, int numAttributes,
		std::vector<std::vector<float>>& cluster_centres,
		std::vector<std::vector<float>>& cluster_centres_parallel) {
	for (int i = 0; i < numClusters; i++)
		for (int j = 0; j < numAttributes; j++)
			if (abs(
					cluster_centres[i][j]
							- cluster_centres_parallel[i][j]) > ACCURACY)
				return false;
	return true;
}

/*---< main() >-------------------------------------------------------------*/
int main(int argc, char **argv) {
	int opt;
	extern char *optarg;
	extern int optind;
	int numClusters = 5;
	char *filename = 0;
	std::vector<std::vector<float>> attributes, attributes_parallel;
	std::vector<std::vector<float>> cluster_centres, cluster_centres_parallel;
	int i, j;

	int numAttributes;
	int numObjects;
	char line[1024];
	int nloops;
	float threshold = 0.001;
	double timing = 0, timing_parallel = 0;
	Timer seq, par;

	while ((opt = getopt(argc, argv, "i:k:t:b")) != EOF) {
		switch (opt) {
		case 'i':
			filename = optarg;
			break;
		case 'b':
			break;
		case 't':
			threshold = atof(optarg);
			break;
		case 'k':
			numClusters = atoi(optarg);
			break;
		case '?':
			usage(argv[0]);
			break;
		default:
			usage(argv[0]);
			break;
		}
	}

	if (filename == 0)
		usage(argv[0]);

	numAttributes = numObjects = 0;

	/* from the input file, get the numAttributes and numObjects ------------*/

	FILE *infile;
	if ((infile = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		exit(1);
	}
	while (fgets(line, 1024, infile) != NULL)
		if (strtok(line, " \t\n") != 0)
			numObjects++;
	rewind(infile);
	while (fgets(line, 1024, infile) != NULL) {
		if (strtok(line, " \t\n") != 0) {
			/* ignore the id (first attribute): numAttributes = 1; */
			while (strtok(NULL, " ,\t\n") != NULL)
				numAttributes++;
			break;
		}
	}

	/* allocate space for attributes[] and read attributes of all objects */
	rewind(infile);
	i = 0;
	while (fgets(line, 1024, infile) != NULL) {
		if (strtok(line, " \t\n") == NULL)
			continue;
		attributes.resize(attributes.size() + 1);
		attributes_parallel.resize(attributes_parallel.size() + 1);

		for (j = 0; j < numAttributes; j++) {
			float val = atof(strtok(NULL, " ,\t\n"));
			attributes[i].push_back(val);
			attributes_parallel[i].push_back(val);
		}
		i++;
	}
	fclose(infile);

	nloops = 1;

	seq.startMeasurement();
	for (i = 0; i < nloops; i++) {
		cluster_centres = cluster(numObjects, numAttributes, attributes, // [numObjects][numAttributes]
				numClusters, threshold);
	}
	seq.endMeasurement();
	timing = seq.getMilliseconds();

	par.startMeasurement();
	for (i = 0; i < nloops; i++) {
		cluster_centres_parallel = cluster_parallel(numObjects, numAttributes,
				attributes_parallel, // [numObjects][numAttributes]
				numClusters, threshold);
	}
	cudaDeviceSynchronize();
	par.endMeasurement();
	timing_parallel = par.getMilliseconds();

	std::cout << "Entry file being processed: " << filename << std::endl;

	/*printf("Cluster Centers Output\n");
	 printf(
	 "The first number is cluster number and the following data is arribute value\n");
	 printf(
	 "=============================================================================\n\n");



	 for (i = 0; i < numClusters; i++) {
	 printf("%d: ", i);
	 for (j = 0; j < numAttributes; j++)
	 printf("%f ", cluster_centres[i][j]);
	 printf("\n\n");
	 }
	 */
	std::cout << "Sequential processing time is " << timing << " ms."
			<< std::endl;
	std::cout << "Parallel processing time is " << timing_parallel << " ms."
			<< std::endl;
	std::cout << "Speedup is " << timing / timing_parallel * 100 << "%!"
			<< std::endl;

	if (isTestPassed(numClusters, numAttributes, cluster_centres,
			cluster_centres_parallel))
		std::cout << "Test passed!" << std::endl;
	else
		std::cout << "Test failed!" << std::endl;

	std::cout << std::endl;
	return (0);
}

/*---< cluster() >-----------------------------------------------------------*/
std::vector<std::vector<float>> cluster(int numObjects, int numAttributes,
		std::vector<std::vector<float>>& attributes, int numClusters,
		float threshold) {

	std::vector<int> membership;

	return kmeans_clustering(attributes, numAttributes, numObjects, numClusters,
			threshold, membership);

}

/*---< cluster_parallel() >-----------------------------------------------------------*/
std::vector<std::vector<float>> cluster_parallel(int numObjects,
		int numAttributes, std::vector<std::vector<float>>& attributes_parallel,
		int numClusters, float threshold) {

	std::vector<int> membership;

	return kmeans_clustering_parallel(attributes_parallel, numAttributes,
			numObjects, numClusters, threshold, membership);
}
