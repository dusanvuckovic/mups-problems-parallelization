#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include "mpi.h"

const double ACCURACY = 0.01;

#define MINREAL -1024.0
#define MAXREAL 1024.0

/**
 * Multiply matrices in parallel using MPI and N-version programming and test the
 * performance gain.
*/

typedef enum {
	Undefined,
	MainCoordinator,
	GroupACoordinator,
	GroupAMember,
	GroupBCoordinator,
	GroupBMember
} belongs;

static int groupACoordinatorGlobal = -1, groupBCoordinatorGlobal = -1;

int worldSize = -1, worldRank = -1;
int groupRank = -1;
int groupASize = -1, groupBSize = -1;

belongs belongsTo = Undefined;

MPI_Datatype Matrix, Matrix_Row, Matrix_Column, Matrix_Column_Send;
MPI_Group groupA, groupB, mainGroup;
MPI_Comm commA, commB;

int* groupAElements, *groupBElements;

int getMin(int a, int b) {
	if (a > b)
		return b;
	return a;
}

void matFillSimple(long N, double *mat, double val) {
	long i, j;

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			mat[i * N + j] = val;
}

void matFillRand(long N, double *mat) {
	long i, j;

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			mat[i * N + j] = (rand() / (double) RAND_MAX)
					* (MAXREAL - MINREAL)+ MINREAL;
}

void matFillTriangle(long N, double *mat) {
	long i, j;

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++) {
			if (i < j)
				mat[i * N + j] = (rand() / (double) RAND_MAX) * (MAXREAL - MINREAL) + MINREAL;
			else
				mat[i * N + j] = 0.0;
		}
}

void matMul(long N, double *a, double *b, double *c) {
	long i, j, k;

	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			for (k = 0; k < N; k++)
				c[i * N + j] += a[i * N + k] * b[k * N + j];
}

bool matricesEqual(long N, double* A, double *B) {
	bool isSuccessful = true;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			if (abs(A[i * N + j] - B[i * N + j]) > ACCURACY) {
				printf("Difference in %i: %.2f | %.2f\n", i*N+j, A[i*N+j], B[i*N+j]);
				isSuccessful = false;
			}
	return isSuccessful;
}

int getAPartialSize(long N) {
	if (groupASize == 1)
		return N;
	return ((N + groupASize - 1) / groupASize);
}

int getBPartialSize(long N) {
	if (groupBSize == 1)
		return N;
	return ((N + groupBSize - 1) / groupBSize);
}

void allocatePositionsAndSizes(const long N, int numberOfElementsPerProcess, int groupSize, int** positions, int** sizes) {

	*sizes = (int*) malloc(sizeof(int)*groupSize);
	*positions = (int*) malloc(sizeof(int)*groupSize);
	int remainingElements = N, offset = 0, iterationElements;

	for (int i = 0; i < groupSize; i++) {
		iterationElements = getMin(remainingElements, numberOfElementsPerProcess);

		(*sizes)[i] = iterationElements;
		(*positions)[i] = offset;

		remainingElements -= numberOfElementsPerProcess;
		offset += iterationElements;
	}
}

void matMulMPIMaster(long N, double *A, double *B, double *C, double *C_A, double* C_B) {
	int i, j, k;

	MPI_Send(A, 1, Matrix, groupACoordinatorGlobal, 0, MPI_COMM_WORLD);
	MPI_Send(B, 1, Matrix, groupACoordinatorGlobal, 0, MPI_COMM_WORLD);

	MPI_Send(A, 1, Matrix, groupBCoordinatorGlobal, 0, MPI_COMM_WORLD);
	MPI_Send(B, 1, Matrix, groupBCoordinatorGlobal, 0, MPI_COMM_WORLD);

	MPI_Recv(C_A, 1, Matrix, groupACoordinatorGlobal, 0, MPI_COMM_WORLD, NULL);
	MPI_Recv(C_B, 1, Matrix, groupBCoordinatorGlobal, 0, MPI_COMM_WORLD, NULL);

	if (matricesEqual(N, C_A, C))
		printf("N-version test, group A, successful!\n");
	else
		printf("N-version test, group A, failed!\n");

	if (matricesEqual(N, C_B, C))
		printf("N-version test, group B, successful!\n");
	else
		printf("N-version test, group B, failed!\n");

}

void matMulMPIGroupACoordinator(const long N) {
	long i, j, k;
	int size, position;
	double *A, *B, *C, *APrime, *CPrime;
	A = (double *) malloc(N * N * sizeof(double));
	B = (double *) malloc(N * N * sizeof(double));
	C = (double *) malloc(N * N * sizeof(double));

	int *positions, *sizes;
	size = getAPartialSize(N);
	allocatePositionsAndSizes(N, size, groupASize, &positions, &sizes);
	size = sizes[groupRank];
	position = positions[groupRank];

	APrime = (double *) malloc(N * size * sizeof(double));
	CPrime = (double *) malloc(N * size * sizeof(double));

	// receive A and B from the global coordinator
	MPI_Recv(A, 1, Matrix, 0, 0, MPI_COMM_WORLD, NULL);
	MPI_Recv(B, 1, Matrix, 0, 0, MPI_COMM_WORLD, NULL);

	// send an equally large chunk of A to all A processes
	MPI_Scatterv(A, sizes, positions, Matrix_Row, APrime, size, Matrix_Row, 0, commA);
	// send the entirety of B to all A processes
	MPI_Bcast(B, 1, Matrix, 0, commA);

	for (i = 0; i < size; i++)
		for (j = 0; j < N; j++)
			for (k = 0; k < N; k++)
				CPrime[i * N + j] += APrime[i * N + k] * B[k * N + j];


	MPI_Gatherv(CPrime, size, Matrix_Row, C, sizes, positions, Matrix_Row, 0, commA);

	MPI_Send(C, 1, Matrix, 0, 0, MPI_COMM_WORLD);

	free(A);
	free(B);
	free(C);
	free(APrime);
	free(CPrime);
}


void matMulMPIGroupBCoordinator(const long N) {
	long i, j, k;
	int size, position;
	double *A, *B, *C, *BPrime, *CPrime;
	A = (double *) malloc(N * N * sizeof(double));
	B = (double *) malloc(N * N * sizeof(double));
	C = (double *) malloc(N * N * sizeof(double));

	int *positions, *sizes;
	size = getBPartialSize(N);
	allocatePositionsAndSizes(N, size, groupBSize, &positions, &sizes);
	size = sizes[groupRank];
	position = positions[groupRank];

	BPrime = (double *) malloc(N * size * sizeof(double));
	CPrime = (double *) malloc(N * size * sizeof(double));

	// receive A and B from the global coordinator
	MPI_Recv(A, 1, Matrix, 0, 0, MPI_COMM_WORLD, NULL);
	MPI_Recv(B, 1, Matrix, 0, 0, MPI_COMM_WORLD, NULL);

	// send an equally large chunk of B to all B processes
	MPI_Scatterv(B, sizes, positions, Matrix_Column, BPrime, size, Matrix_Row, 0, commB);

	// send the entirety of A to all B processes
	MPI_Bcast(A, 1, Matrix, 0, commB);

	for (j = 0; j < size; j++)
		for (i = 0; i < N; i++)
			for (k = 0; k < N; k++)
				CPrime[j * N + i] += A[i * N + k] * BPrime[j * N + k];


	MPI_Gatherv(CPrime, size, Matrix_Row, C, sizes, positions, Matrix_Column, 0, commB);

	MPI_Send(C, 1, Matrix, 0, 0, MPI_COMM_WORLD);

	free(A);
	free(B);
	free(C);
	free(BPrime);
	free(CPrime);

}
void matMulMPIGroupAMember(const long N) {

	long i, j, k;
	int size, position;
	double *B, *APrime, *CPrime;

	B = (double *) malloc(N * N * sizeof(double));

	int *positions, *sizes;
	size = getAPartialSize(N);
	allocatePositionsAndSizes(N, size, groupASize, &positions, &sizes);
	size = sizes[groupRank];
	position = positions[groupRank];

	APrime = (double *) malloc(N * size * sizeof(double));
	CPrime = (double *) malloc(N * size * sizeof(double));

	// accept a part of A
	MPI_Scatterv(NULL, sizes, positions, Matrix_Column, APrime, size, Matrix_Column, 0, commA);
	// acept the entirety of B
	MPI_Bcast(B, 1, Matrix, 0, commA);

	for (i = 0; i < size; i++)
		for (j = 0; j < N; j++)
			for (k = 0; k < N; k++)
				CPrime[i * N + j] += APrime[i * N + k] * B[k * N + j];

	MPI_Gatherv(CPrime, size, Matrix_Column, NULL, NULL, NULL, Matrix_Column, 0, commA);

	free(B);
	free(APrime);
	free(CPrime);
}

void matMulMPIGroupBMember(const long N) {

	long i, j, k;
	int size, position;
	double *A, *BPrime, *CPrime;

	A = (double *) malloc(N * N * sizeof(double));

	int *positions, *sizes;
	size = getBPartialSize(N);
	allocatePositionsAndSizes(N, size, groupBSize, &positions, &sizes);
	size = sizes[groupRank];
	position = positions[groupRank];

	BPrime = (double *) malloc(N * size * sizeof(double));
	CPrime = (double *) malloc(N * size * sizeof(double));

	// accept a part of B
	MPI_Scatterv(NULL, sizes, positions, Matrix_Column, BPrime, size, Matrix_Row, 0, commB);

	// accept the entirety of A
	MPI_Bcast(A, 1, Matrix, 0, commB);

	for (j = 0; j < size; j++)
		for (i = 0; i < N; i++)
			for (k = 0; k < N; k++)
				CPrime[j * N + i] += A[i * N + k] * BPrime[j * N + k];

	MPI_Gatherv(CPrime, size, Matrix_Row, NULL, NULL, NULL, Matrix_Column, 0, commB);

	free(A);
	free(BPrime);
	free(CPrime);

}

void determineBelonging(const int firstBProcess) {
	if (!worldRank)
		belongsTo = MainCoordinator;
	else if (worldRank < firstBProcess) {
		MPI_Group_rank(groupA, &groupRank);
		if (!groupRank)
			belongsTo = GroupACoordinator;
		else
			belongsTo = GroupAMember;
	}
	else {
		MPI_Group_rank(groupB, &groupRank);
		if (!groupRank)
			belongsTo = GroupBCoordinator;
		else
			belongsTo = GroupBMember;
	}
}

void matMulMPISlave(long N) {
	switch (belongsTo) {
		case GroupACoordinator: matMulMPIGroupACoordinator(N); break;
		case GroupBCoordinator: matMulMPIGroupBCoordinator(N); break;
		case GroupAMember: 		matMulMPIGroupAMember(N); break;
		case GroupBMember: 		matMulMPIGroupBMember(N); break;
	}
}

int main(int argc, char **argv) {

	MPI_Init(&argc, &argv);

	long N;
	double *A, *B, *C, *C_A, *C_B, t, tMPI;

	if (argc > 1) {
		N = atoi(argv[1]);
	} else {
		N = 256;
	}

	MPI_Type_contiguous(N*N, MPI_DOUBLE, &Matrix);
	MPI_Type_contiguous(N, MPI_DOUBLE, &Matrix_Row);
	MPI_Type_vector(N, 1, N, MPI_DOUBLE, &Matrix_Column_Send);

	MPI_Type_commit(&Matrix);
	MPI_Type_commit(&Matrix_Row);
	MPI_Type_commit(&Matrix_Column_Send);
	MPI_Type_create_resized(Matrix_Column_Send, 0, sizeof(MPI_DOUBLE), &Matrix_Column);
	MPI_Type_commit(&Matrix_Column);

	MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
	MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);

	MPI_Comm_group(MPI_COMM_WORLD, &mainGroup);

	int counter = 1, firstBProcess;
	groupASize = worldSize / 2, groupBSize = worldSize / 2;

	if (!(worldSize % 2))
		--groupASize;
	groupAElements = malloc(sizeof(int) * groupASize);
	groupBElements = malloc(sizeof(int) * groupBSize);

	for (int i = 0; i < groupASize; i++)
		groupAElements[i] = counter++;

	firstBProcess = counter;

	for (int i = 0; i < groupBSize; i++)
		groupBElements[i] = counter++;

	MPI_Group_incl(mainGroup, groupASize, groupAElements, &groupA);
	MPI_Group_incl(mainGroup, groupBSize, groupBElements, &groupB);

	MPI_Comm_create(MPI_COMM_WORLD, groupA, &commA);
	MPI_Comm_create(MPI_COMM_WORLD, groupB, &commB);

	determineBelonging(firstBProcess);

	if (belongsTo == GroupACoordinator) {
		groupACoordinatorGlobal = worldRank;
		for (int i = 0; i < worldSize; i++)
			if (i != groupACoordinatorGlobal)
				MPI_Send(&groupACoordinatorGlobal, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
	}
	else
		MPI_Recv(&groupACoordinatorGlobal, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, NULL);


	if (belongsTo == GroupBCoordinator) {
		groupBCoordinatorGlobal = worldRank;
		for (int i = 0; i < worldSize; i++)
			if (i != groupBCoordinatorGlobal)
				MPI_Send(&groupBCoordinatorGlobal, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
	}
	else
		MPI_Recv(&groupBCoordinatorGlobal, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, NULL);

	srand(time(NULL));

	if (belongsTo == MainCoordinator) {
		A = (double *) malloc(N * N * sizeof(double));
		B = (double *) malloc(N * N * sizeof(double));
		C = (double *) malloc(N * N * sizeof(double));
		C_A = (double *) malloc(N * N * sizeof(double));
		C_B = (double *) malloc(N * N * sizeof(double));
		matFillRand(N, A);
		matFillRand(N, B);

		t = MPI_Wtime();
		matMul(N, A, B, C);
		t = MPI_Wtime() - t;

		tMPI = MPI_Wtime();

		matMulMPIMaster(N, A, B, C, C_A, C_B);

		tMPI = MPI_Wtime() - tMPI;
		fprintf(stdout, "Sequential execution done in %lf ms.\n", t*1000);
	    fprintf(stdout, "Parallel execution done in %lf ms.\n", tMPI*1000);
	    fprintf(stdout, "A %ldx%ld matrix reached speedup: %lf%%.\n", N, N, t/tMPI*100);

		if (matricesEqual(N, C, C_A) && matricesEqual(N, C, C_B))
			fprintf(stdout, "Test PASSED!\n");
		else
			fprintf(stdout, "Test FAILED!\n");

		fflush(stdout);

		free(A);
		free(B);
		free(C);
		free(C_A);
		free(C_B);

	} else
		matMulMPISlave(N);

	free(groupAElements);
	free(groupBElements);

	MPI_Group_free(&groupA);
	MPI_Group_free(&groupB);

	if (belongsTo == GroupACoordinator || belongsTo == GroupAMember)
		MPI_Comm_free(&commA);
	if (belongsTo == GroupBCoordinator || belongsTo == GroupBMember)
		MPI_Comm_free(&commB);

	MPI_Type_free(&Matrix);
	MPI_Type_free(&Matrix_Row);
	MPI_Type_free(&Matrix_Column);
	MPI_Type_free(&Matrix_Column_Send);
	MPI_Finalize();

	return EXIT_SUCCESS;
}

