#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <utility>
#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#include "mpi.h"

#define M_SEED 9

/**
 * Process the pathfinder problem (find shortest path in matrix) 
 * in parallel using MPI and test the performance gain.
*/

int rows, cols;
int* data;
int** wall;

int* result, *result_parallel;

double parallelTime, sequentialTime;

uint worldSize, worldRank;
bool internalGood = true;

int* columnBuffer;

int elementsPerRow, elementsPerColumn;

MPI::Datatype Matrix;
MPI::Datatype Matrix_Row;
MPI::Datatype Matrix_Column, Matrix_Column_Send;
MPI::Datatype Submatrix_First, Submatrix_Middle, Submatrix_Last;

MPI::Aint intLb, intExtent;

void initializeData() {
    data = new int[rows * cols];
    wall = new int*[rows];
    for (int n = 0; n < rows; n++)
    	wall[n] = data + cols * n;
    result = new int[cols];
    result_parallel = new int[cols];

    int seed = M_SEED;
    srand(seed);

    for (auto i = 0; i < rows; i++)
        for (auto j = 0; j < cols; j++)
        	wall[i][j] = rand() % 10;

    for (auto j = 0; j < cols; j++)
    	result[j] = result_parallel[j] = wall[0][j];
}

void cleanupData() {
    delete[] data;
    delete[] wall;
}

bool isMaster() {
	return !worldRank;
}

void processSequential() {
    int *source, *destination, *tmp_destination;
    int min;

    tmp_destination = destination = result;
    source = new int[cols];

    auto time = MPI::Wtime();
    for (auto t = 0; t < rows - 1; t++) {
        std::swap<int*>(source, destination);
        for (auto n = 0; n < cols; n++) {
            int min = source[n];
            if (n > 0)
                min = std::min(min, source[n - 1]);
            if (n < cols - 1)
                min = std::min(min, source[n + 1]);

            destination[n] = wall[t + 1][n] + min;
        }
    }
    sequentialTime = MPI::Wtime() - time;
    for (int i = 0; i < cols; i++)
        result[i] = destination[i];

    printf("\n");
    printf("A %dx%d matrix processed by %d threads sequentially in %.2f ms.\n", rows, cols, worldSize, sequentialTime*1000);
    if (source != tmp_destination)
        	delete [] source;
}

void processParallel() {

	auto time = MPI::Wtime();

	std::vector<int> columnCount, algoColumnCount, startingColumn, algoStartingColumn;
	columnCount.resize(worldSize); algoColumnCount.resize(worldSize);
	startingColumn.resize(worldSize); algoStartingColumn.resize(worldSize);

	int myNumberOfColumns = columnCount[worldRank], myNumberOfAlgoColumns = algoColumnCount[worldRank],
			startingPosition = startingColumn[worldRank], algoStartingPosition = algoStartingColumn[worldRank];

	int* myMatrix;

	if (isMaster()) {
			MPI::COMM_WORLD.Bcast(data, cols, Matrix_Column_Send, 0);
			myMatrix = data;
	}
	else {
		myMatrix = new int[rows*cols];
		MPI::COMM_WORLD.Bcast(myMatrix, cols, Matrix_Column_Send, 0);
	}

    int *source = new int[cols], *destination = new int[cols];

    for (int i = 0; i < cols; i++)
    	destination[i] = myMatrix[i]; // analogous to filling result

    for (auto t = 0; t < rows - 1; t++) {
        std::swap<int*>(source, destination);
        for (auto n = 0; n < cols; n++) {
            int min = source[n];
            if (n > 0)
                min = std::min(min, source[n - 1]);
            if (n < cols-1)
                min = std::min(min, source[n + 1]);
            destination[n] = myMatrix[(t+1)*cols + n] + min;
        }
    }

    if (!isMaster())
    	MPI::COMM_WORLD.Send(destination, cols, MPI::INT, 0, 0);
    else {

    	for (int i = 0; i < cols; i++)
    		result_parallel[i] = destination[i];

    	int *dstcmp = new int[cols];
    	for (int i = 1; i < worldSize; i++) {
    		MPI::COMM_WORLD.Recv(dstcmp, cols, MPI::INT, i, 0);
    		internalGood &= !memcmp(dstcmp, destination, cols*sizeof(int));
    	}
    	delete [] dstcmp;
    	memcpy(result_parallel, destination, cols*sizeof(int));
    }

    parallelTime = MPI::Wtime() - time;
    if (isMaster())
    	printf("A %dx%d matrix processed by %d threads in parallel in %.2f ms.\n", rows, cols, worldSize, parallelTime*1000);
}


bool compareResults() {
	bool returnVal = true;
    for (auto i = 0; i < cols; i++)
        if (result[i] != result_parallel[i])
        	 returnVal = false;
    return returnVal;
}

int main(int argc, char** argv) {

	MPI::Init(argc, argv);

    worldSize = MPI::COMM_WORLD.Get_size();
    worldRank = MPI::COMM_WORLD.Get_rank();

    if (argc != 3) {
    	printf("Usage: pathfinder width num_of_steps\n");
    	exit(EXIT_FAILURE);
    }

	cols = atoi(argv[1]);
	rows = atoi(argv[2]);

	elementsPerRow = cols;
	elementsPerColumn = rows;

	MPI::INT.Get_extent(intLb, intExtent); // previous function is deprecated

	Matrix = MPI::INT.Create_contiguous(rows*cols);
	Matrix_Row = MPI::INT.Create_contiguous(elementsPerRow);
	Matrix_Column = MPI::INT.Create_vector(rows, 1, cols);

	Matrix.Commit();
	Matrix_Row.Commit();
	Matrix_Column.Commit();

	Matrix_Column_Send = Matrix_Column.Create_resized(0, 1*sizeof(int));
	Matrix_Column_Send.Commit();

    if (isMaster()) {
    	initializeData();
    	processSequential();
    	processParallel();
    	if (internalGood)
    	    std::cout << "Internal consistency valid!" << std::endl;
    	else
    	 	std::cout << "Internal consistency invalid!" << std::endl;
    	if (compareResults()) {
    		std::cout << "TEST PASSED!" << std::endl;
    		std::cout << "Speedup is " << sequentialTime/parallelTime*100 << "%." << std::endl;
    	}
    	else
    		std::cout << "TEST FAILED!" << std::endl;
    	cleanupData();

    }
    else
    	processParallel();

    Matrix.Free();
    Matrix_Row.Free();
    Matrix_Column.Free();
    Matrix_Column_Send.Free();
    MPI::Finalize();
    return EXIT_SUCCESS;
}


