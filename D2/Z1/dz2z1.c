#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include "mpi.h"

const double ACCURACY = (0.01);

#define MPI_SEND_TAG (0)
#define MPI_RECV_TAG (0)
#define MPI_MASTER (0)

#define MINREAL -1024.0
#define MAXREAL 1024.0

int worldSize = -1, worldRank = -1;

/**
 * Multiply matrices in parallel using MPI and test the
 * performance gain.
*/

void matFillSimple(long N, double *mat, double val) {
   long i, j;

   for(i = 0; i < N; i ++)
      for(j = 0; j < N; j ++)
         mat[i * N + j] = val;
}

void matFillRand(long N, double *mat) {
   long i, j;

   for(i = 0; i < N; i ++)
      for(j = 0; j < N; j ++)
         mat[i * N + j] = (rand() / (double) RAND_MAX)*(MAXREAL - MINREAL) + MINREAL;
}

void matFillTriangle(long N, double *mat) {
   long i, j;

   for(i = 0; i < N; i ++)
      for(j = 0; j < N; j ++) { 
         if (i < j) mat[i * N + j] = (rand() / (double) RAND_MAX)*(MAXREAL - MINREAL) + MINREAL;
         else  mat[i * N + j] = 0.0;
      }
}

void matMul(long N, double *a, double *b, double *c) {
   long i, j, k;

   for (i = 0; i < N; i ++)
      for (j = 0; j < N; j ++)
         for (k = 0; k < N; k ++)
            c[i * N + j] += a[i * N + k] * b[k * N + j];
}

void matMulMPIMaster(long N, double *a, double *b, double *c) {
   int i, j, k;
   double** matrixBuffer;
   int numRows = (N + worldSize-1) / (worldSize);
   matrixBuffer = malloc((sizeof(double*))*(worldSize-1));
   for (i = 0; i < worldSize-1; i++)
	   matrixBuffer[i] = malloc(sizeof(double)*N*numRows);


   for (i = 1; i < worldSize; i++) {
	   MPI_Send(a + numRows*i*N, numRows*N, MPI_DOUBLE, i, MPI_SEND_TAG, MPI_COMM_WORLD);
	   MPI_Send(b, N*N, MPI_DOUBLE, i, MPI_SEND_TAG, MPI_COMM_WORLD);
   }

   for (i = 0; i < numRows; i++)
   		for (j = 0; j < N; j++)
   			for (k = 0; k < N; k ++)
   				c[i*N + j] += a[i * N + k] * b[k * N + j];


   for (i = 0; i < worldSize-1; i++)
	   MPI_Recv(matrixBuffer[i], N*numRows, MPI_DOUBLE, i+1, MPI_RECV_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);


   for (i = 1; i < worldSize; i++)
	   for (j = 0; j < N*numRows; j++)
		   c[i*N*numRows + j] = matrixBuffer[i-1][j];

}

void matMulMPISlave(long N) {
	int i, j, k;
	double *a, *b, *c;
	int numRows = (N + worldSize-1) / (worldSize);
	int start = worldRank*numRows;
	int end = start + numRows;
	a = malloc(sizeof(double)*N*numRows);
	b = malloc(sizeof(double)*N*N);
	c = malloc(sizeof(double)*N*numRows);
	MPI_Recv(a, numRows*N, MPI_DOUBLE, MPI_MASTER, MPI_RECV_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	MPI_Recv(b, N*N, MPI_DOUBLE, MPI_MASTER, MPI_RECV_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	int cnt = 0;
	for (i = 0; i < numRows; i ++)
		for (j = 0; j < N; j ++) {
			for (k = 0; k < N; k ++)
				c[cnt] += a[i * N + k] * b[k * N + j];
			++cnt;
		}

	MPI_Send(c, numRows*N, MPI_DOUBLE, MPI_MASTER, MPI_SEND_TAG, MPI_COMM_WORLD);
}

bool matricesEqual(long N, double* A, double *B) {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			if (abs(A[i*N + j] - B[i*N + j]) > ACCURACY)
				return false;
	return true;
}

bool isMaster(int rank) {
	return !rank;
}

int main(int argc, char **argv) {

   MPI_Init(&argc, &argv);


   long N;
   double *A, *B, *C, *CMPI, t, tMPI;

   MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
   MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);

   srand(time(NULL));

   if (argc > 1) {
      N = atoi(argv[1]);
   } else {
      N = 256;
   }
   if (isMaster(worldRank)) {
	   A = (double *) malloc(N * N * sizeof(double));
	   B = (double *) malloc(N * N * sizeof(double));
	   C = (double *) malloc(N * N * sizeof(double));
	   CMPI = (double *) malloc(N * N * sizeof(double));
	   matFillRand(N, A);
	   matFillRand(N, B);

	   t = MPI_Wtime();
	   matMul(N, A, B, C);
	   t = MPI_Wtime() - t;

	   tMPI = MPI_Wtime();

	   matMulMPIMaster(N, A, B, CMPI);

	   tMPI = MPI_Wtime() - tMPI;
	   fprintf(stdout, "Sequential execution done in %lf ms.\n", t*1000);
	   fprintf(stdout, "Parallel execution done in %lf ms.\n", tMPI*1000);
	   fprintf(stdout, "A %ldx%ld matrix reached speedup: %lf%%.\n", N, N, t/tMPI*100);
   	   if (matricesEqual(N, C, CMPI))
   		   fprintf(stdout, "Test PASSED!\n");
   	   else
            fprintf(stdout, "Test FAILED!\n");

   	   fflush(stdout);

   	   free(A);
   	   free(B);
   	   free(C);
   }
   else
	   matMulMPISlave(N);

   MPI_Finalize();

   return EXIT_SUCCESS;
}
