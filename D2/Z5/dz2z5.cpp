#include <cstddef> // for offsetof
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <deque>
#include "mpi.h"


/**
 * Process the graph traversal problem in parallel using MPI and test the
 * performance gain.
*/

int worldRank, worldSize;

//Structure holding node information
struct Node {
	int starting;
	int no_of_edges;
};

int numberOfNodes;
int edge_list_size;
FILE *fp;

MPI::Datatype JobType;

//sequential data structures
Node* h_graph_nodes = nullptr;
bool *h_graph_mask = nullptr;
bool *h_updating_graph_mask = nullptr;
bool *h_graph_visited = nullptr;
int32_t *h_graph_edges = nullptr;
int32_t *h_cost = nullptr;

//parallel data structures
Node* h_graph_nodes_parallel = nullptr;
bool* h_graph_mask_parallel = nullptr;
bool* h_updating_graph_mask_parallel = nullptr;
bool* h_graph_visited_parallel = nullptr;
int32_t *h_graph_edges_parallel = nullptr;
int32_t *h_cost_parallel = nullptr;

/* Note: std::vector<bool> avoided due to the bitwise optimization */

bool isMaster() {
	return !worldRank;
}

void readAndInitializeData(const std::string& filename) {
	//printf("Reading File\n"); //Read in graph from a file
	fp = fopen(filename.c_str(), "r");
	if (!fp) {
		printf("Error reading graph file!\n");
		printf("TEST FAILED!\n");
		exit(EXIT_FAILURE);
	}

	int source = 0, error = 1;
	error = fscanf(fp, "%d", &numberOfNodes); //read the total number of nodes
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	h_graph_nodes = new Node[numberOfNodes];
	h_graph_mask = new bool[numberOfNodes];
	h_updating_graph_mask = new bool[numberOfNodes];
	h_graph_visited = new bool[numberOfNodes];
	h_cost = new int32_t[numberOfNodes];

	h_graph_nodes_parallel = new Node[numberOfNodes];
	h_graph_mask_parallel = new bool[numberOfNodes];
	h_updating_graph_mask_parallel = new bool[numberOfNodes];
	h_graph_visited_parallel = new bool[numberOfNodes];
	h_cost_parallel = new int32_t[numberOfNodes];

	unsigned int start, edgeno;
	for (auto i = 0; i < numberOfNodes; i++) {
		error = fscanf(fp, "%d %d", &start, &edgeno);
		h_graph_nodes[i].starting = h_graph_nodes_parallel[i].starting = start;
		h_graph_nodes[i].no_of_edges = h_graph_nodes_parallel[i].no_of_edges =
				edgeno;
		h_graph_mask[i] = h_graph_mask_parallel[i] = false;
		h_updating_graph_mask[i] = h_updating_graph_mask_parallel[i] = false;
		h_graph_visited[i] = h_graph_visited_parallel[i] = false;
		h_cost[i] = h_cost_parallel[i] = -1;
	}
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	//read the source node from the file
	error = fscanf(fp, "%d", &source);
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}
	source = 0;

	//set the source node as true in the mask
	h_graph_mask[source] = h_graph_mask_parallel[source] = true;
	h_graph_visited[source] = h_graph_visited[source] = true;

	error = fscanf(fp, "%d", &edge_list_size);
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	int32_t id, cost;
	h_graph_edges = new int32_t[edge_list_size];
	h_graph_edges_parallel = new int32_t[edge_list_size];
	for (int i = 0; i < edge_list_size; i++) {
		error = fscanf(fp, "%d", &id);
		error = fscanf(fp, "%d", &cost);
		h_graph_edges[i] = h_graph_edges_parallel[i] = id;
	}
	if (!error) {
		printf("Error in reading file!\n");
		exit(EXIT_FAILURE);
	}

	if (fp)
		fclose(fp);
}

void destroyData() {
	delete[] h_graph_nodes;
	delete[] h_graph_nodes_parallel;
	delete[] h_graph_mask;
	delete[] h_graph_mask_parallel;
	delete[] h_updating_graph_mask;
	delete[] h_updating_graph_mask_parallel;
	delete[] h_graph_visited;
	delete[] h_graph_visited_parallel;
	delete[] h_graph_edges;
	delete[] h_graph_edges_parallel;
	delete[] h_cost;
	delete[] h_cost_parallel;

}

void processSequential() {
	bool stop;
	do {
		stop = false; //if no thread changes this value then the loop stops

		for (auto tid = 0; tid < numberOfNodes; tid++)
			if (h_graph_mask[tid] == true) {
				h_graph_mask[tid] = false;
				for (auto i = h_graph_nodes[tid].starting;
						i
								< (h_graph_nodes[tid].no_of_edges
										+ h_graph_nodes[tid].starting); i++) {
					auto id = h_graph_edges[i];
					if (!h_graph_visited[id]) {
						h_cost[id] = h_cost[tid] + 1;
						h_updating_graph_mask[id] = true;
					}
				}
			}

		for (auto tid = 0; tid < numberOfNodes; tid++)
			if (h_updating_graph_mask[tid] == true) {
				h_graph_mask[tid] = true;
				h_graph_visited[tid] = true;
				stop = true;
				h_updating_graph_mask[tid] = false;
			}
	} while (stop);
}

struct Job {

	//input
	int32_t tid; // outside iteration parameter
	int32_t id; //id = h_graph_edges_parallel[i];
	bool h_graph_visited; //condition
	int32_t h_cost_input; //h_cost_parallel[tid]

	//output
	int32_t h_cost_output = 0; //h_cost_parallel[id] = h_cost_parallel[tid] + 1;
	bool h_updating_graph_mask = false; //h_updating_graph_mask_parallel[id] = true;

	Job() :
			tid(0), id(0), h_graph_visited(h_graph_visited), h_cost_input(
					h_cost_input) {
	}
	Job(int32_t tid, int32_t id, bool h_graph_visited, int32_t h_cost_input) :
			tid(tid), id(id), h_graph_visited(h_graph_visited), h_cost_input(
					h_cost_input) {
	}

	~Job() = default;
	Job(Job&) = default;
	Job(Job&&) = default;
	Job& operator =(Job&) = default;

	bool editsMade() const {
		return !h_graph_visited;
	}

	bool isFinalSignal() const {
		return tid == -1;
	}

	void process() {
		if (!h_graph_visited) {
			h_cost_output = h_cost_input + 1;
			h_updating_graph_mask = true;
		}
	}
};

void createStructuredDatatype() {

	const int jobDifferentFields = 6;
	int jobElementCounts[] = { 1, 1, 1, 1, 1, 1 };
	MPI::Datatype jobFieldTypes[] = { MPI::INT, MPI::INT, MPI::BOOL, MPI::INT,
			MPI::INT, MPI::BOOL };
	MPI::Aint jobFieldDisplacements[] = { offsetof(Job, tid), offsetof(Job, id),
			offsetof(Job, h_graph_visited), offsetof(Job, h_cost_input),
			offsetof(Job, h_cost_output), offsetof(Job, h_updating_graph_mask) };

	JobType = MPI::Datatype::Create_struct(jobDifferentFields, jobElementCounts,
			jobFieldDisplacements, jobFieldTypes);
	JobType.Commit();
}

void processParallelSlave() {
	bool stop;
	Job j;

	while (!stop) {
		MPI::COMM_WORLD.Recv(&j, 1, JobType, 0, 0);
		if (j.isFinalSignal())
			stop = true;
		else {
			j.process();
			MPI::COMM_WORLD.Send(&j, 1, JobType, 0, 0);
		}
	}
}

void processParallel() {

	bool stop;
	std::deque<Job> myJobs;
	uint32_t counter;

	do {
		counter = 0;
		stop = false; //if no thread changes this value then the loop stops

		for (auto tid = 0; tid < numberOfNodes; tid++)
			if (h_graph_mask_parallel[tid] == true) {
				h_graph_mask_parallel[tid] = false;
				for (auto i = h_graph_nodes_parallel[tid].starting;
						i < (h_graph_nodes_parallel[tid].no_of_edges + h_graph_nodes_parallel[tid].starting);
						i++) {

					auto id = h_graph_edges_parallel[i];
					Job j = { tid, id, h_graph_visited_parallel[id],
							h_cost_parallel[tid] };
					MPI::COMM_WORLD.Send(&j, 1, JobType, counter % worldSize, 0);
					++counter;
				}
			}

		int size = counter;
		counter = 0;

		for (int i = 0; i < size; i++) {
			Job jq = { 0, 0, 0, 0 };
			MPI::COMM_WORLD.Recv(&jq, 1, JobType, counter % worldSize, 0);
			jq.process();
			++counter;
			if (jq.editsMade()) {
				h_cost_parallel[jq.id] = jq.h_cost_output;
				h_updating_graph_mask_parallel[jq.id] =
						jq.h_updating_graph_mask;
			}
		}

		for (int tid = 0; tid < numberOfNodes; tid++) {
			if (h_updating_graph_mask_parallel[tid] == true) {
				h_graph_mask_parallel[tid] = true;
				h_graph_visited_parallel[tid] = true;
				stop = true;
				h_updating_graph_mask_parallel[tid] = false;
			}

		}
	} while (stop);

	Job j = {-1, 0, 0, 0};
	for (int i = 1; i < worldSize; i++)
		MPI::COMM_WORLD.Send(&j, 1, JobType, i, 0);

}

bool compareResults() {
	for (auto i = 1; i < numberOfNodes; i++)
		if (h_cost[i] != h_cost_parallel[i])
			return false;
	return true;
}

void storeToFiles() { // Store the result into files
	FILE *fpo = fopen("resultSequential.txt", "w");
	for (auto i = 0; i < numberOfNodes; i++)
		fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
	fclose(fpo);
	printf("Result stored in resultSequential.txt\n");

	FILE *fpop = fopen("resultParallel.txt", "w");
	for (int i = 0; i < numberOfNodes; i++)
		fprintf(fpop, "%d) cost:%d\n", i, h_cost_parallel[i]);
	fclose(fpop);
	printf("Result stored in resultParallel.txt\n");
}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {

	MPI::Init(argc, argv);

	numberOfNodes = 0;
	edge_list_size = 0;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_file> \n", argv[0]);
		exit(EXIT_FAILURE);
	}

	worldSize = MPI::COMM_WORLD.Get_size();
	worldRank = MPI::COMM_WORLD.Get_rank();

	createStructuredDatatype();

	readAndInitializeData(argv[1]);

	if (isMaster()) {
		auto sequentialTime = MPI::Wtime();
		processSequential();
		sequentialTime = MPI::Wtime() - sequentialTime;
		std::cout << "Sequential implementation done in "
			<< sequentialTime * 1000 << " ms." << std::endl;

		auto parallelTime = MPI::Wtime();
		processParallel();
		parallelTime = MPI::Wtime() - parallelTime;
		std::cout << "Parallel implementation done in " << parallelTime * 1000
			<< " ms." << std::endl;

		std::cout << "Speedup:  "
			<< sequentialTime / parallelTime * 100 << "%." << std::endl;

		if (compareResults()) {
			std::cout << "Test PASSED!" << std::endl;
		} else
			std::cout << "Test FAILED!" << std::endl;
		storeToFiles();
		destroyData();
	}
	else
		processParallelSlave();

	JobType.Free();
	MPI::Finalize();
}
